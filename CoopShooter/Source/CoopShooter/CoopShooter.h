// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#define SURFACE_FLESH_DEFAULT SurfaceType1
#define SURFACE_FLESH_VULNERABLE SurfaceType2
#define SURFACE_NONE SurfaceType62

#define CHANNEL_WEAPON ECC_GameTraceChannel1

