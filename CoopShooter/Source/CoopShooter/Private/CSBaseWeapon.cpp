// Fill out your copyright notice in the Description page of Project Settings.


#include "CSBaseWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"

// Sets default values
ACSBaseWeapon::ACSBaseWeapon()
{
    SkeletalMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComp"));
    RootComponent = SkeletalMeshComp;

    MuzzleSocketName = "MuzzleSocket";

    BulletsPerMinute = 120.0f;

    SetReplicates(true);
}

void ACSBaseWeapon::BeginFire()
{
    float TimeBetweenBullets = 60.0f / BulletsPerMinute;
    float StartDelay = FMath::Max(0.0f, LastFireTime + TimeBetweenBullets - GetWorld()->TimeSeconds);
    GetWorldTimerManager().SetTimer(AutoFireTimerHandler, this, &ACSBaseWeapon::Fire, TimeBetweenBullets, true, StartDelay);
}

void ACSBaseWeapon::StopFire()
{
    GetWorldTimerManager().ClearTimer(AutoFireTimerHandler);
}

void ACSBaseWeapon::Fire()
{
    LastFireTime = GetWorld()->TimeSeconds;
}

void ACSBaseWeapon::PlayMuzzleEffect()
{
    if (MuzzleEffect)
        UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, SkeletalMeshComp, MuzzleSocketName);
}
