// Fill out your copyright notice in the Description page of Project Settings.


#include "CSGameInstance.h"
#include "OnlineSessionSettings.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/NetDriver.h"

const static FName SESSION_NAME = TEXT("CSGameSession");
const static FName SERVER_NAME_SETTINGS_KEY = TEXT("ServerName");

void UCSGameInstance::Init()
{
    ClientBattle = false;

    IOnlineSubsystem* SubSystem = IOnlineSubsystem::Get();
    if (!SubSystem)
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::Init !SubSystem"));
        return;
    }

    SessionInterface = SubSystem->GetSessionInterface();
    if (!SessionInterface.IsValid())
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::Init !SessionInterface.IsValid()"));
        return;
    }

    SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UCSGameInstance::OnCreateSessionComplete);
    SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UCSGameInstance::OnDestroySessionComplete);
    SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UCSGameInstance::OnFindSessionsComplete);
    SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UCSGameInstance::OnJoinSessionsComplete);

    GetEngine()->OnNetworkFailure().AddUObject(this, &UCSGameInstance::OnNetworkFailure);

    GetEngine()->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("UCSGameInstance::Init"));
}

void UCSGameInstance::PlaySinglePlayer()
{
    UWorld* World = GetWorld();
    if (!World)
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::OnCreateSessionComplete !World"));
        return;
    }

    World->ServerTravel(InURL);
}

void UCSGameInstance::Host(FString ServerName)
{
    if (!SessionInterface.IsValid())
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::Host !SessionInterface.IsValid()"));
        return;
    }

    auto ExistingSession = SessionInterface->GetNamedSession(SESSION_NAME);
    if (ExistingSession)
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::Host ExistingSession"));
        return;
    }

    FOnlineSessionSettings SessionSettings;
    SessionSettings.bIsLANMatch = false;
    SessionSettings.NumPublicConnections = 2;
    SessionSettings.bShouldAdvertise = true;
    SessionSettings.bUsesPresence = true;
    SessionSettings.Set(SERVER_NAME_SETTINGS_KEY, ServerName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

    SessionInterface->CreateSession(0, SESSION_NAME, SessionSettings);

    GetEngine()->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("UCSGameInstance::Host"));
}

void UCSGameInstance::DestroySession()
{
    if (!SessionInterface.IsValid())
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::DestroySession !SessionInterface.IsValid()"));
        return;
    }

    auto ExistingSession = SessionInterface->GetNamedSession(SESSION_NAME);
    if (!ExistingSession)
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::DestroySession ExistingSession"));
        return;
    }

    SessionInterface->DestroySession(SESSION_NAME);

    GetEngine()->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("UCSGameInstance::DestroySession"));
}

void UCSGameInstance::FindSessions()
{
    if (!SessionInterface.IsValid())
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::FindSessions !SessionInterface.IsValid()"));
        return;
    }

    SessionSearch = MakeShareable(new FOnlineSessionSearch());
    if (!SessionSearch.IsValid())
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::FindSessions !SessionSearch.IsValid()"));
        return;
    }

    SessionSearch->MaxSearchResults = 100;
    SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
    SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());

    GetEngine()->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("UCSGameInstance::FindSessions"));
}

void UCSGameInstance::JoinSession(int32 Idx)
{
    if (!SessionInterface.IsValid())
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::JoinSession !SessionInterface.IsValid()"));
        return;
    }

    if (!SessionSearch.IsValid())
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::JoinSession !SessionSearch.IsValid()"));
        return;
    }

    if (Idx >= SessionSearch->SearchResults.Num())
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::JoinSession Idx >= SessionSearch->SearchResults.Num()"));
        return;
    }

    SessionInterface->JoinSession(0, SESSION_NAME, SessionSearch->SearchResults[Idx]);

    GetEngine()->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("UCSGameInstance::JoinSession"));
}

void UCSGameInstance::LeaveBattle()
{
    ClientBattle = false;

    if (!SessionInterface.IsValid())
    {
        UGameplayStatics::OpenLevel(GetWorld(), MainMeuURL, true);
        return;
    }
    auto ExistingSession = SessionInterface->GetNamedSession(SESSION_NAME);
    if (!ExistingSession)
    {
        UGameplayStatics::OpenLevel(GetWorld(), MainMeuURL, true);
        return;
    }

    SessionInterface->DestroySession(SESSION_NAME);
}

void UCSGameInstance::OnCreateSessionComplete(FName SessionName, bool Success)
{
    if (!Success)
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::OnCreateSessionComplete !Success"));
        return;
    }

    UWorld* World = GetWorld();
    if (!World)
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::OnCreateSessionComplete !World"));
        return;
    }

    World->ServerTravel(InURL);

    GetEngine()->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("UCSGameInstance::OnCreateSessionComplete"));
}

void UCSGameInstance::OnDestroySessionComplete(FName SessionName, bool Success)
{
    GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Orange, TEXT("UCSGameInstance::OnDestroySessionComplete"));
    UGameplayStatics::OpenLevel(GetWorld(), MainMeuURL, true);
}

void UCSGameInstance::OnFindSessionsComplete(bool Success)
{
    if (!SessionSearch.IsValid())
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::OnFindSessionsComplete !SessionSearch.IsValid()"));
        return;
    }

    if (!Success)
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::OnFindSessionsComplete !Success"));
        return;
    }

    OnBeginMainMenuSessionList();
    int32 Idx = 0;
    for (const FOnlineSessionSearchResult& SearchResult : SessionSearch->SearchResults)
    {
        FServerData Data;
        FString ServerName;
        if (SearchResult.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, ServerName))
        {
            Data.Name = ServerName;
        }
        else
        {
            Data.Name = "Could not find name";
        }
        Data.MaxPlayers = SearchResult.Session.SessionSettings.NumPublicConnections;
        Data.CurrentPlayers = Data.MaxPlayers - SearchResult.Session.NumOpenPublicConnections;
        Data.HostUsername = SearchResult.Session.OwningUserName;

        OnAddMainMenuSessionList(Data);
    }
    OnFinishMainMenuSessionList();

    GetEngine()->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("UCSGameInstance::OnFindSessionsComplete"));
}

void UCSGameInstance::OnJoinSessionsComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
    if (Result != EOnJoinSessionCompleteResult::Type::Success)
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::OnJoinSessionsComplete Result != EOnJoinSessionCompleteResult::Type::Success"));
        return;
    }

    if (!SessionInterface.IsValid())
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::OnJoinSessionsComplete !SessionInterface.IsValid()"));
        return;
    }

    FString Url;
    if (!SessionInterface->GetResolvedConnectString(SESSION_NAME, Url))
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::OnJoinSessionsComplete !SessionInterface->GetResolvedConnectString"));
        return;
    }

    APlayerController* PlayerController = GetFirstLocalPlayerController();
    if (!PlayerController)
    {
        GetEngine()->AddOnScreenDebugMessage(0, 5, FColor::Red, TEXT("UCSGameInstance::OnJoinSessionsComplete !PlayerController"));
        return;
    }

    PlayerController->ClientTravel(Url, ETravelType::TRAVEL_Absolute);

    ClientBattle = true;

    GetEngine()->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("UCSGameInstance::OnJoinSessionsComplete"));
}

void UCSGameInstance::OnNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType,
    const FString& ErrorString)
{
    GetEngine()->AddOnScreenDebugMessage(0, 2, FColor::Red, ErrorString);
    if (ClientBattle)
        LeaveBattle();
}
