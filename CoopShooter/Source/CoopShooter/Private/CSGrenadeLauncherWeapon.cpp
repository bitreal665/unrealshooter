// Fill out your copyright notice in the Description page of Project Settings.


#include "CSGrenadeLauncherWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/Actor.h"

ACSGrenadeLauncherWeapon::ACSGrenadeLauncherWeapon()
{

}

void ACSGrenadeLauncherWeapon::Fire()
{
    Super::Fire();

    LastShootInfo.Dirty++;
    OnRep_LastShootInfo();
    ServerFire();
}

void ACSGrenadeLauncherWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME_CONDITION(ACSGrenadeLauncherWeapon, LastShootInfo, COND_SkipOwner);
}

void ACSGrenadeLauncherWeapon::ServerFire_Implementation()
{
    AActor* MyActor = GetOwner();
    if (ProjectileClass && MyActor)
    {
        FVector MuzzleLocation = SkeletalMeshComp->GetSocketLocation(MuzzleSocketName);
        FRotator MuzzleRotation = SkeletalMeshComp->GetSocketRotation(MuzzleSocketName);

        //Set Spawn Collision Handling Override
        FActorSpawnParameters ActorSpawnParams;
        ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
        ActorSpawnParams.Instigator = GetInstigator();
        ActorSpawnParams.Owner = MyActor;

        FVector EyeLocation;
        FRotator EyeRotation;
        MyActor->GetActorEyesViewPoint(EyeLocation, EyeRotation);

        // spawn the projectile at the muzzle
        GetWorld()->SpawnActor<AActor>(ProjectileClass, MuzzleLocation, EyeRotation, ActorSpawnParams);

        LastShootInfo.Dirty++;
        if (GetNetMode() == NM_ListenServer && !MyActor->HasLocalNetOwner())
            OnRep_LastShootInfo();
    }
}

void ACSGrenadeLauncherWeapon::OnRep_LastShootInfo()
{
    PlayMuzzleEffect();
}