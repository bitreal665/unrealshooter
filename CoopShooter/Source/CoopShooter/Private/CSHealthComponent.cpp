// Fill out your copyright notice in the Description page of Project Settings.


#include "CSHealthComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Controller.h"
#include "Net/UnrealNetwork.h"
#include "CSTeamMember.h"

void UCSHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UCSHealthComponent, Health);
}

void UCSHealthComponent::Heal(float healValue)
{
    UpdateHealth(GetOwner(), -healValue, nullptr, nullptr, nullptr);
}

// Sets default values for this component's properties
UCSHealthComponent::UCSHealthComponent()
{
    StartHealth = 100.0f;
    MaxHealth = 100.0f;
    Health = StartHealth;
    bIsDead = false;

    SetIsReplicated(true);
}


// Called when the game starts
void UCSHealthComponent::BeginPlay()
{
	Super::BeginPlay();

    Health = StartHealth;
    bIsDead = Health <= 0.0f;

    if (GetOwnerRole() == ROLE_Authority)
    {
        AActor* MyActor = GetOwner();
        if (MyActor)
        {
            MyActor->OnTakeAnyDamage.AddDynamic(this, &UCSHealthComponent::OnTakeAnyDamage);
        }
    }
}

void UCSHealthComponent::OnRep_Health()
{
    bool bPrevIsDead = bIsDead;
    bIsDead = Health <= 0.0f;

    OnHealthChanged.Broadcast(this, Health, 0, nullptr, nullptr, nullptr, nullptr);
    if (bIsDead != bPrevIsDead)
        OnIsDeadChanged.Broadcast(this, Health, 0, nullptr, nullptr, nullptr, nullptr);
}

void UCSHealthComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, 
    AController* InstigatedBy, AActor* DamageCauser)
{
    if (DamagedActor != DamageCauser && UCSTeamMember::AreFriends(DamagedActor, DamageCauser))
        return;

    UpdateHealth(DamagedActor, Damage, DamageType, InstigatedBy, DamageCauser);
}

void UCSHealthComponent::UpdateHealth(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
    AController* InstigatedBy, AActor* DamageCauser)
{
    float NewHealth = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);

    if (FMath::Abs(NewHealth - Health) < 0.01f)
        return;

    bool bPrevIsDead = bIsDead;
    Health = NewHealth;
    bIsDead = Health <= 0.0f;

    OnHealthChanged.Broadcast(this, Health, Damage, DamagedActor, DamageType, InstigatedBy, DamageCauser);
    if (bIsDead != bPrevIsDead)
        OnIsDeadChanged.Broadcast(this, Health, Damage, DamagedActor, DamageType, InstigatedBy, DamageCauser);
}

bool UCSHealthComponent::IsAlive(AActor* Actor)
{
    if (!Actor)
        return false;
    UCSHealthComponent* HealthComp = Cast<UCSHealthComponent>(Actor->GetComponentByClass(UCSHealthComponent::StaticClass()));
    if (!HealthComp)
        return false;
    return !HealthComp->bIsDead;
}

