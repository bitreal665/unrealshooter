// Fill out your copyright notice in the Description page of Project Settings.


#include "CSTeamMember.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UCSTeamMember::UCSTeamMember()
{
    TeamId = 0;
}

int UCSTeamMember::GetRelationship(int TeamA, int TeamB)
{
    if (TeamA == 0 || TeamB == 0)
        return 0;
    if (TeamA == TeamB)
        return 1;
    return -1;
}

int UCSTeamMember::GetRelationship(AActor* ActorA, AActor* ActorB)
{
    if (!ActorA || !ActorB)
        return 0;
    UCSTeamMember* TeamMemberA = Cast<UCSTeamMember>(ActorA->GetComponentByClass(UCSTeamMember::StaticClass()));
    UCSTeamMember* TeamMemberB = Cast<UCSTeamMember>(ActorB->GetComponentByClass(UCSTeamMember::StaticClass()));
    if (!TeamMemberA || !TeamMemberB)
        return 0;
    return GetRelationship(TeamMemberA->TeamId, TeamMemberB->TeamId);
}

bool UCSTeamMember::AreFriends(AActor* ActorA, AActor* ActorB)
{
    return GetRelationship(ActorA, ActorB) == 1;
}

bool UCSTeamMember::AreEnemies(AActor* ActorA, AActor* ActorB)
{
    return GetRelationship(ActorA, ActorB) == -1;
}

bool UCSTeamMember::AreNeutrals(AActor* ActorA, AActor* ActorB)
{
    return GetRelationship(ActorA, ActorB) == 0;
}
