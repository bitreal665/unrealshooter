// Fill out your copyright notice in the Description page of Project Settings.


#include "CSWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "CoopShooter.h"
#include "Net/UnrealNetwork.h"

static int32 DebugWeaponDrawing = 0;
FAutoConsoleVariableRef CVARDebugWeaponDrawing(
    TEXT("COOP.DebugWeaponDrawing"),
    DebugWeaponDrawing,
    TEXT("draw debug lines for weapons"),
    EConsoleVariableFlags::ECVF_Cheat);

// Sets default values
ACSWeapon::ACSWeapon()
{
    BaseDamage = 20.0f;

    TracerTargetName = "Target";

    HitImpulsePower = 1000.0f;

    BulletSpread = 1.0f;
}

void ACSWeapon::Fire()
{
    Super::Fire();

    AActor* MyActor = GetOwner();
    if (MyActor)
    {
        FVector ImpactPoint;
        FVector ImpactNormal;
        UPrimitiveComponent* HitComp = nullptr;
        EPhysicalSurface HitSurfaceType = SURFACE_NONE;

        FVector EyeLocation;
        FRotator EyeRotation;
        MyActor->GetActorEyesViewPoint(EyeLocation, EyeRotation);
        FVector ShotDir = EyeRotation.Vector();
        float HalfRad = FMath::DegreesToRadians(BulletSpread);
        ShotDir = FMath::VRandCone(ShotDir, HalfRad, HalfRad);
        FVector TraceEnd = EyeLocation + ShotDir * 10000;
        ImpactPoint = TraceEnd;

        FHitResult Hit;
        FCollisionQueryParams QueryParams;
        QueryParams.AddIgnoredActor(MyActor);
        QueryParams.AddIgnoredActor(this);
        QueryParams.bTraceComplex = true;
        QueryParams.bReturnPhysicalMaterial = true;
        if (GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, CHANNEL_WEAPON, QueryParams))
        {
            ImpactPoint = Hit.ImpactPoint;
            ImpactNormal = Hit.ImpactNormal;
            HitComp = Hit.GetComponent();
            HitSurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());

            if (DebugWeaponDrawing > 0)
            {
                DrawDebugLine(GetWorld(), EyeLocation, TraceEnd, FColor::White, false, 1.0f, 0, 1.0f);
            }
        }

        LastShootInfo.HitSurfaceType = HitSurfaceType;
        LastShootInfo.ImpactPoint = ImpactPoint;
        LastShootInfo.ImpactNormal = ImpactNormal;
        LastShootInfo.Dirty++;
        OnRep_LastShootInfo();
        ServerFire(HitComp, HitSurfaceType, ImpactPoint, ImpactNormal);
    }
}

void ACSWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME_CONDITION(ACSWeapon, LastShootInfo, COND_SkipOwner);
}

void ACSWeapon::ServerFire_Implementation(UPrimitiveComponent* HitComp, EPhysicalSurface HitSurfaceType, const FVector_NetQuantize& ImpactPoint, const FVector_NetQuantize& ImpactNormal)
{
    AActor* MyActor = GetOwner();
    if (MyActor)
    {
        if (HitComp)
        {
            float ActualDamage = BaseDamage;
            if (HitSurfaceType == SURFACE_FLESH_VULNERABLE)
                ActualDamage *= 3.0f;
            FVector ShotDir;
            FHitResult Hit;
            UGameplayStatics::ApplyPointDamage(HitComp->GetOwner(), ActualDamage, ShotDir, Hit,
                MyActor->GetInstigatorController(), MyActor, DamageType);
            if (HitComp->IsSimulatingPhysics())
                HitComp->AddImpulseAtLocation(-ImpactNormal * HitImpulsePower, ImpactPoint);
        }

        LastShootInfo.HitSurfaceType = HitSurfaceType;
        LastShootInfo.ImpactPoint = ImpactPoint;
        LastShootInfo.ImpactNormal = ImpactNormal;
        LastShootInfo.Dirty++;
        if (GetNetMode() == NM_ListenServer && !MyActor->HasLocalNetOwner())
            OnRep_LastShootInfo();
    }
}

void ACSWeapon::OnRep_LastShootInfo()
{
    PlayMuzzleEffect();

    if (TracerEffect)
    {
        FVector MuzzleLocation = SkeletalMeshComp->GetSocketLocation(MuzzleSocketName);
        UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, MuzzleLocation);
        if (TracerComp)
        {
            TracerComp->SetVectorParameter(TracerTargetName, LastShootInfo.ImpactPoint);
        }
    }

    if (LastShootInfo.HitSurfaceType != SURFACE_NONE)
    {
        UParticleSystem* ImpactEffect;
        switch (LastShootInfo.HitSurfaceType)
        {
        case SURFACE_FLESH_DEFAULT:
        case SURFACE_FLESH_VULNERABLE:
            ImpactEffect = BloodImpactEffect;
            break;
        default:
            ImpactEffect = DefaultImpactEffect;
            break;
        }
        if (ImpactEffect)
            UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, LastShootInfo.ImpactPoint, 
                LastShootInfo.ImpactNormal.Rotation());
    }

    OnFire();
}
