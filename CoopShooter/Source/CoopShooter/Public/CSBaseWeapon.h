// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CSBaseWeapon.generated.h"

class USkeletalMeshComponent;
class UParticleSystem;
struct FTimerHandle;

UCLASS()
class COOPSHOOTER_API ACSBaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
    ACSBaseWeapon();

    UFUNCTION(BlueprintCallable, Category = "Weapon")
        void BeginFire();

    UFUNCTION(BlueprintCallable, Category = "Weapon")
        void StopFire();

    UFUNCTION(BlueprintImplementableEvent, Category = "Weapon")
        void OnFire();

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
        USkeletalMeshComponent* SkeletalMeshComp;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
        UParticleSystem* MuzzleEffect;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
        FName MuzzleSocketName;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
        float BulletsPerMinute;

    virtual void Fire();

    void PlayMuzzleEffect();

private:
    FTimerHandle AutoFireTimerHandler;
    float LastFireTime;

};
