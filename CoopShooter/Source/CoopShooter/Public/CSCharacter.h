// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CSCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UCSHealthComponent;

UCLASS()
class COOPSHOOTER_API ACSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACSCharacter();

    virtual FVector GetPawnViewLocation() const override;

protected:
	// Called when the game starts or when spawned
    virtual void BeginPlay() override;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
        USpringArmComponent* SpringArmComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
        UCameraComponent* CameraComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
        UCSHealthComponent* HealthComp;

    UFUNCTION(BlueprintImplementableEvent, Category = "Player")
        void OnIsDead();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
    void MoveForward(float Value);
    void MoveRight(float Value);

    void BeginCrouch();
    void EndCrouch();

    UFUNCTION()
        void OnIsDeadChanged(UCSHealthComponent* HealthCompSender,
            float Health,
            float Delta,
            AActor* DamagedActor,
            const UDamageType* DamageType,
            AController* InstigatedBy,
            AActor* DamageCauser);

};
