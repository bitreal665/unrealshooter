// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionInterface.h"
#include "CSGameInstance.generated.h"

class UNetDriver;


USTRUCT(BlueprintType)
struct FServerData
{
    GENERATED_USTRUCT_BODY()

public:
    UPROPERTY(BlueprintReadOnly)
        FString Name;
    UPROPERTY(BlueprintReadOnly)
        int32 CurrentPlayers;
    UPROPERTY(BlueprintReadOnly)
        int32 MaxPlayers;
    UPROPERTY(BlueprintReadOnly)
        FString HostUsername;
};


/**
 *
 */
UCLASS()
class COOPSHOOTER_API UCSGameInstance : public UGameInstance
{
	GENERATED_BODY()

private:
    IOnlineSessionPtr SessionInterface;
    TSharedPtr<class FOnlineSessionSearch> SessionSearch;
    bool ClientBattle;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "CSGameInstance")
        FString InURL;
    UPROPERTY(EditDefaultsOnly, Category = "CSGameInstance")
        FName MainMeuURL;

public:
    virtual void Init() override;

    UFUNCTION(BlueprintCallable, Category = "CSGameInstance")
        void PlaySinglePlayer();
    UFUNCTION(BlueprintCallable, Category = "CSGameInstance")
        void Host(FString ServerName);
    UFUNCTION(BlueprintCallable, Category = "CSGameInstance")
        void DestroySession();
    UFUNCTION(BlueprintCallable, Category = "CSGameInstance")
        void FindSessions();
    UFUNCTION(BlueprintCallable, Category = "CSGameInstance")
        void JoinSession(int32 Idx);
    UFUNCTION(BlueprintCallable, Category = "CSGameInstance")
        void LeaveBattle();

protected:
    UFUNCTION(BlueprintImplementableEvent, Category = "CSGameInstance")
        void OnBeginMainMenuSessionList();
    UFUNCTION(BlueprintImplementableEvent, Category = "CSGameInstance")
        void OnAddMainMenuSessionList(const FServerData& ServerData);
    UFUNCTION(BlueprintImplementableEvent, Category = "CSGameInstance")
        void OnFinishMainMenuSessionList();

private:
    void OnCreateSessionComplete(FName SessionName, bool Success);
    void OnDestroySessionComplete(FName SessionName, bool Success);
    void OnFindSessionsComplete(bool Success);
    void OnJoinSessionsComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

    void OnNetworkFailure(UWorld * World, UNetDriver * NetDriver, ENetworkFailure::Type FailureType, const FString & ErrorString);

};
