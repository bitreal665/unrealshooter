// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CSBaseWeapon.h"
#include "CSGrenadeLauncherWeapon.generated.h"

class ACSGrenade;
class AActor;


USTRUCT()
struct FGrenadeLauncherShootInfo
{
    GENERATED_BODY()

public:
    UPROPERTY()
        uint8 Dirty;    // TODO: I don't know how to mark field as dirty
};


UCLASS()
class COOPSHOOTER_API ACSGrenadeLauncherWeapon : public ACSBaseWeapon
{
	GENERATED_BODY()
	
public:
    ACSGrenadeLauncherWeapon();

    virtual void Fire() override;

    void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Weapon")
        TSubclassOf<AActor> ProjectileClass;

    UFUNCTION(Server, Reliable)
        void ServerFire();

    UPROPERTY(ReplicatedUsing = OnRep_LastShootInfo)
        FGrenadeLauncherShootInfo LastShootInfo;

    UFUNCTION()
        void OnRep_LastShootInfo();
};
