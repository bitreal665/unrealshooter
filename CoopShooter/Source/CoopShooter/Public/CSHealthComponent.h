// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CSHealthComponent.generated.h"


class AActor;
class AController;


DECLARE_DYNAMIC_MULTICAST_DELEGATE_SevenParams(FOnHealthChangedSignature,
    UCSHealthComponent*, HealthComp,
    float, Health,
    float, Delta,
    AActor*, DamagedActor,
    const UDamageType*, DamageType,
    AController*, InstigatedBy,
    AActor*, DamageCauser);


DECLARE_DYNAMIC_MULTICAST_DELEGATE_SevenParams(FOnIsDeadChangedSignature,
    UCSHealthComponent*, HealthComp,
    float, Health,
    float, Delta,
    AActor*, DamagedActor,
    const UDamageType*, DamageType,
    AController*, InstigatedBy,
    AActor*, DamageCauser);


UCLASS( ClassGroup=(CoopShooter), meta=(BlueprintSpawnableComponent) )
class COOPSHOOTER_API UCSHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UCSHealthComponent();

    UPROPERTY(BlueprintReadOnly, Category = "Health", ReplicatedUsing=OnRep_Health)
        float Health;

    UPROPERTY(BlueprintReadOnly, Category = "Health")
        bool bIsDead;

    void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    UFUNCTION(BlueprintCallable, Category = "Health")
    void Heal(float healValue);

protected:

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health")
        float StartHealth;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health")
        float MaxHealth;

	virtual void BeginPlay() override;

    UFUNCTION()
        void OnRep_Health();

private:

    UFUNCTION()
        void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, 
            class AController* InstigatedBy, AActor* DamageCauser);

    void UpdateHealth(AActor* DamagedActor, float NewHealth, const UDamageType* DamageType,
        AController* InstigatedBy, AActor* DamageCauser);

public:

    UPROPERTY(BlueprintAssignable, Category = "Events")
        FOnHealthChangedSignature OnHealthChanged;

    UPROPERTY(BlueprintAssignable, Category = "Events")
        FOnIsDeadChangedSignature OnIsDeadChanged;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Health")
        static bool IsAlive(AActor* Actor);

};
