// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CSTeamMember.generated.h"


class AActor;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class COOPSHOOTER_API UCSTeamMember : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCSTeamMember();

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Team")
        int TeamId;

public:

    int GetTeamId() const { return TeamId; }

    //UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Team")
        static int GetRelationship(int TeamA, int TeamB);

    //UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Team")
        static int GetRelationship(AActor* ActorA, AActor* ActorB);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Team")
        static bool AreFriends(AActor* ActorA, AActor* ActorB);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Team")
        static bool AreEnemies(AActor* ActorA, AActor* ActorB);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Team")
        static bool AreNeutrals(AActor* ActorA, AActor* ActorB);
};
