// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CSBaseWeapon.h"
#include "CSWeapon.generated.h"

class USkeletalMeshComponent;
class UDamageType;
class UParticleSystem;


USTRUCT()
struct FRifleShootInfo
{
    GENERATED_BODY()

public:
    UPROPERTY()
        TEnumAsByte<EPhysicalSurface> HitSurfaceType;
    UPROPERTY()
        FVector_NetQuantize ImpactPoint;
    UPROPERTY()
        FVector_NetQuantize ImpactNormal;
    UPROPERTY()
        uint8 Dirty;    // TODO: I don't know how to mark field as dirty
};


UCLASS()
class COOPSHOOTER_API ACSWeapon : public ACSBaseWeapon
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACSWeapon();

    virtual void Fire() override;

    void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon", meta = (ClampMin=0.0f))
        float BulletSpread;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
        float BaseDamage;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
        TSubclassOf<UDamageType> DamageType;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
        UParticleSystem* DefaultImpactEffect;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
        UParticleSystem* BloodImpactEffect;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
        FName TracerTargetName;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
        UParticleSystem* TracerEffect;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
        float HitImpulsePower;

    UFUNCTION(Server, Reliable)
    void ServerFire(UPrimitiveComponent* HitComp, EPhysicalSurface HitSurfaceType, const FVector_NetQuantize& ImpactPoint, const FVector_NetQuantize& ImpactNormal);    // TODO: need to use net-types

    UPROPERTY(ReplicatedUsing=OnRep_LastShootInfo)
        FRifleShootInfo LastShootInfo;

    UFUNCTION()
    void OnRep_LastShootInfo();

};
