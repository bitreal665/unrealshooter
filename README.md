It's a simple coop shooter game powered by UE4 that is the final project from UE4 course https://www.udemy.com/course/unrealengine-cpp/

Short video - https://youtu.be/YPNER5D6aF8

# How to run

You need UE 4.23

## How to run in UE Editor

Open ***TestMap*** level

Click ***Play***

## How to run builded game

Build project in UE4

### How to run multiplayer

Run 2 game instances on 2 different PC with different Steam accounts

The first player should create a game. Click ***Start Session*** in the main menu

The second player should connect to the game. Click ***Find Session*** in the main menu then select a session and click ***Join***

### How to run single player

Click ***Start single player game*** in the main menu

# Features

Logic is implemented with C++ and blueprints

Coop multiplayer support

AI is implemented with behaviour trees and EQS queries

Added matchmaking with Steam